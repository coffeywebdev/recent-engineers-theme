<?php


	function recent_engineers_theme_support() {

		/* post formats */
		add_theme_support( 'post-formats', array( 'aside', 'quote' ) );

		/* post thumbnails */
		add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );

		/* HTML5 */
		add_theme_support( 'html5' );

		/* automatic feed links */
		add_theme_support( 'automatic-feed-links' );

		load_child_theme_textdomain( 'recent-engineers', get_stylesheet_directory() . '/languages' );

		register_nav_menus( array(
			'main' => __( 'Main Menu', 'recent-engineers' ),
			'footer' => __( 'Footer Links', 'recent-engineers' ),
		) );

		add_filter('show_admin_bar', '__return_false');

	}
	add_action( 'after_setup_theme', 'recent_engineers_theme_support' );



function recent_engineers_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'recentengineers-font', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700', array(), null );
	wp_enqueue_style( 'recentengineers-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons', array(), null );
	wp_enqueue_style( 'recentengineers-theme', get_theme_file_uri('/css/materialize.css'), array(), null );
	wp_enqueue_style( 'recentengineers-theme-overwrite', get_theme_file_uri('/style.css'), array(), null );


	// Theme stylesheet.

	wp_enqueue_script('greensock-api','https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js', array('jquery'),null,true);
	wp_enqueue_script( 'materialize', get_theme_file_uri( '/js/materialize.js' ), array('jquery'), null, true );
	wp_register_script( 'core', get_theme_file_uri( '/js/init.js' ), array('jquery','materialize'), null, true );
	$args = array(
		'ajax_url' => admin_url('admin-ajax.php'),
	);
	wp_localize_script('core', 'localized', $args);
	wp_enqueue_script('core');



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'recent_engineers_scripts' );


/* theme / plugin action hooks */
//add_action('wp_enqueue_scripts', 'include_scripts');
add_action('wp_ajax_plugin2theme_job_search', 'ajax_job_search');
add_action('wp_ajax_nopriv_plugin2theme_job_search', 'ajax_job_search');
add_shortcode('custom_jobsearch', 'jobSearchFormMarkup');
add_action('init', 'custom_disciplines_taxonomy');


function searchFiltersMarkup(){
	$html = "<form id='jobSearch'>
			          <div class='search-filters'>
			            <div class='search-filters-inner'>	
			              <div class='input-field col s12 l4'>
			                <select id='search-type' multiple name='search-type' tabindex=''-1' aria-hidden='true'>
			                  <option value='internship'>Internship</option>
			                  <option value='full-time'>Full Time</option>
			                </select>
			                <label for='search-type'>Job Type</label>
			              </div>
			              <div class='input-field col s6 l4'>
			                <select id='search-category' multiple name='search-category' tabindex=''-1' aria-hidden='true'>"
	        .disciplineSearchFilterOptions().
	        "</select>
			                  <label for='search-category'>Job Discipline</label>
			              </div>
			              <div class='input-field col s6 l4'>
			                <select id='search-location' multiple name='search-location' tabindex=''-1' aria-hidden='true'>"
	        .locationSearchFilterOptions().
	        "</select>
			                  <label for='search-location'>Job Location</label>
			              </div>
			            </div>
			          </div>";

//				          <div class='input-field col s12 l12'>
//				            <button class='btn waves-effect waves-light center-align' type='submit' name='action'>Submit
//									    <i class='material-icons right'>send</i>
//									  </button>
//				          </div>
//
	$html .= "<input id='search-page' name='search-page' type='hidden' value='2'></form>";
	return $html;
}

function jobSearchFormMarkup(){
	$html = "<div class='search-holder'>
							    <div class='row'>
							    	<div class='col s12'>
							    		<ul id='search-items-holder' class='collection'>"
	        .searchResultsMarkup().
	        "</ul>
						        </div>
						      </div>".
	        "</div>";

	return $html;
}

function searchResultsMarkup(){
	$html = '';
	$args = array(
		'posts_per_page'   => 10,
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'job_listing',
		'post_status'      => array('published','expired'),
//		'fields'           => '',
	);
	$listings = get_posts($args);
	foreach ($listings as $listing){
		$job_title = $listing->post_title; // for debug . ': ' . $listing->ID;
		$job_link = get_field('job-title-url', $listing->ID);
		$job_expire = get_post_meta( $listing->ID, '_job_expires', true );
		$job_thumbnail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $listing->ID ), 'thumbnail' );
		$company_name = get_post_meta( $listing->ID, '_company_name', true );
		$job_type = get_the_terms( $listing->ID, 'job_listing_type' );
		$job_location = get_the_terms( $listing->ID, 'job_listing_region' );
		$job_type = $job_type[0];
		$job_location = $job_location[0];
		$html .= "<li class='collection-item job'><div class='row'>";
		// job title and company name
		$html .="<div class='col s12 l5'><a class='title' target='_blank' href='{$job_link}'>{$job_title}</a><br />";
		if(!empty($company_name))
			$html .= "<span class='company-name text-gray text-lighten-2'>{$company_name}</span>";
		$html .= "</div>";

		// job region and type
		$html .= "<div class='col s8 l4'>";
		if(!empty($job_location))
			$html .= "<span class='location'>{$job_location->name}</span>";

		$html .= "<br />";

		if(!empty($job_type->name))
			$html .= "<span class='new badge job-type' data-badge-caption=''>{$job_type->name}</span>";
		$html .= "</div>";

		// job expire
		$html .= "<div class='col s4 l3'>";
		if(!empty($job_expire)) {
			$html .= "<div class='mobile-push-up'>";
			$html .= "<span class='desktop-push-left'><small>Expires On</small></span><br />";
			$html .= "<span class=''>{$job_expire}</span>";
			$html .= "</div>";
		}
		$html .= "</div>";
		$html .= "</div></li>";
	}
	return $html;
}

function ajax_job_search(){
	$html = '';
	$jobs = jobSearchQuery($_POST);

	foreach($jobs->posts as $job){
		$job_id = $job->ID;
		$job_title = $job->post_title; // for debug . ': ' . $job->ID;
		// $job_link = get_the_job_permalink($job->ID);
		$job_link = get_field('job-title-url',$job->ID);
		$job_expire = get_post_meta( $job->ID, '_job_expires', true );
		$job_thumbnail_url = wp_get_attachment_image_src( get_post_thumbnail_id( $job->ID ), 'thumbnail' );
		$company_name = get_post_meta( $job->ID, '_company_name', true );
		$job_type = get_the_terms( $job->ID, 'job_listing_type' );
		$job_location = get_the_terms( $job->ID, 'job_listing_region' );
		$job_type = $job_type[0];
		$job_location = $job_location[0];
		$html .= "<li class='collection-item job slide-fade-up'><div class='row'>";
		// job title and company name
		$html .="<div class='col s12 l5'><a class='title' target='_blank' href='{$job_link}'>{$job_title}</a><br />";
		if(!empty($company_name))
			$html .= "<span class='company-name text-gray text-lighten-2'>{$company_name}</span>";
		$html .= "</div>";

		// job region and type
		$html .= "<div class='col s8 l4'>";
		if(!empty($job_location))
			$html .= "<span class='location'>{$job_location->name}</span>";

		$html .= "<br />";

		if(!empty($job_type->name))
			$html .= "<span class='new badge job-type' data-badge-caption=''>{$job_type->name}</span>";
		$html .= "</div>";

		// job expire
		$html .= "<div class='col s4 l3'>";
		if(!empty($job_expire)) {
			$html .= "<div class='mobile-push-up'>";
			$html .= "<span class='desktop-push-left'><small>Expires On</small></span><br />";
			$html .= "<span class=''>{$job_expire}</span>";
			$html .= "</div>";
		}

		$html .= "</div>";
		$html .= "</div></li>";
	}

	echo json_encode(array('success' =>true,'html'=>$html,'post_count'=>$jobs->found_posts,'query'=>$jobs->query));
	wp_die();
}

function custom_disciplines_taxonomy(){
	/* NEX TAXONOMY */
	$labels = array(
		'name'              => _x( 'Disciplines', 'taxonomy general name', 'wp-job-manager-locations' ),
		'singular_name'     => _x( 'Discipline', 'taxonomy singular name', 'wp-job-manager-locations' ),
		'search_items'      => __( 'Search Disciplines', 'wp-job-manager-locations' ),
		'all_items'         => __( 'All Disciplines', 'wp-job-manager-locations' ),
		'parent_item'       => __( 'Parent Discipline', 'wp-job-manager-locations' ),
		'parent_item_colon' => __( 'Parent Discipline:', 'wp-job-manager-locations' ),
		'edit_item'         => __( 'Edit Discipline', 'wp-job-manager-locations' ),
		'update_item'       => __( 'Update Discipline', 'wp-job-manager-locations' ),
		'add_new_item'      => __( 'Add New Discipline', 'wp-job-manager-locations' ),
		'new_item_name'     => __( 'New Discipline Name', 'wp-job-manager-locations' ),
		'menu_name'         => __( 'Discipline', 'wp-job-manager-locations' ),
	);
	$args = array(
		'hierarchical'      => true,
		'update_count_callback' => '_update_post_term_count',
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'job_discipline' ),
	);
	register_taxonomy( 'job_discipline', array( 'job_listing' ), $args );
}

function jobSearchQuery($args){

	$args = wp_parse_args(
		$args,
		array(
			'search_location'   => '',
			'search_category' => '',
			'job_types'         => '',
			'post_status'       => array(),
			'posts_per_page'    => 10,
			'orderby'           => 'date',
			'order'             => 'DESC',
			'featured'          => null,
			'filled'            => null,
			'page'              => 1,
//			'fields'            => 'all',
		)
	);

	if ( ! empty( $args['post_status'] ) ) {
		$post_status = $args['post_status'];
	} elseif ( 0 === intval( get_option( 'job_manager_hide_expired', get_option( 'job_manager_hide_expired_content', 1 ) ) ) ) {
		$post_status = array( 'publish', 'expired' );
	} else {
		$post_status = 'publish';
	}


	$query_args = array(
		'post_type'              => 'job_listing',
		'post_status'            => $post_status,
		'ignore_sticky_posts'    => 1,
		'posts_per_page'         => intval( $args['posts_per_page'] ),
		'orderby'                => $args['orderby'],
		'order'                  => $args['order'],
		'tax_query'              => array(),
		'meta_query'             => array(),
		'paged'                  => intval( $args['page'] ),
		'fields'                 => $args['fields'],
	);

	if ( ! empty( $args['job_types'] ) ) {
		$query_args['tax_query'][] = array(
			'taxonomy' => 'job_listing_type',
			'field'    => 'slug',
			'terms'    => explode(',',$args['job_types']),
		);
	}

	if ( ! empty( $args['search_location'] ) || !empty( $args['search_category'] ) ) {

		$taxonomy_search    = array( 'relation' => 'OR' );

		if(!empty($args['search_location'])) {
			$taxonomy_search[] = array(
				'taxonomy' => 'job_listing_region',
				'field'    => 'slug',
				'terms'    => explode(',', $args['search_location']),
			);
		}

		if(!empty($args['search_category'])) {
			$taxonomy_search[] = array(
				'taxonomy' => 'job_discipline',
				'field'    => 'slug',
				'terms'    => explode(',', $args['search_category']),
			);
		}

		$query_args['tax_query'][] = $taxonomy_search;
	}

	if ( empty( $query_args['meta_query'] ) ) {
		unset( $query_args['meta_query'] );
	}

	if ( empty( $query_args['tax_query'] ) ) {
		unset( $query_args['tax_query'] );
	}

//	var_dump($query_args);
	$result = new WP_Query( $query_args );
	return $result;
}

function disciplineSearchFilterOptions(){
	$terms = get_terms( array(
		'taxonomy' => 'job_discipline',
		'hide_empty' => false,
	) );
	$html = '';
	foreach($terms as $term){
		$html .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
	}
	return $html;
}

function locationSearchFilterOptions(){
	$terms = get_terms( array(
		'taxonomy' => 'job_listing_region',
		'hide_empty' => false,
	) );
	$html = '';
	foreach($terms as $term){
		$html .= '<option value="'.$term->slug.'">'.$term->name.'</option>';
	}
	return $html;
}