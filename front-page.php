<?php
	get_header();
?>
<div class="container white">
  <div class="section">
		<div class="row">
			<div class="col s12">
				<?php echo searchFiltersMarkup(); ?>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<?php echo do_shortcode('[custom_jobsearch]'); ?>
			</div>
      <div class="loading hide">
        <div class="container">
          <div class="progress">
            <div class="indeterminate"></div>
          </div>
        </div>
      </div>
		</div>
	</div>
</div>
<?php
	get_footer();
?>
