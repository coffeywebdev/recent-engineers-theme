(function($){
  $(function(){
    // materialize functions
    $('select').formSelect();
    $('.sidenav').sidenav();

    // when select values change, submit form
    $('#jobSearch select').on('change', function(){
      // if select fields change, set page value to 1 - then trigger form submit
      $('.slide-fade-up').removeClass('slide-fade-up');

      $('#jobSearch').submit();
    });

    // #jobSearch form submit function
    $('#jobSearch').on( "submit", function( event ) {
      event.preventDefault();
      $('.loading').removeClass('hide');
      // console.log( $( this ).serializeArray() );
      var form_data = $(this).serializeArray();
      var tmp = {'search-type': '','search-category': '','search-location': ''};
      form_data.forEach(function(element) {
        if(element.name==='search-type'||element.name==='search-category'||element.name==='search-location'){
          tmp[element.name] += element.value + ',';
        }
      });

      var data = {
        action: 'plugin2theme_job_search',
        job_types: tmp['search-type'],
        search_category: tmp['search-category'],
        search_location: tmp['search-location'],
      }
        console.log(data);
      $.post(localized.ajax_url, data, function(res) {
        var res = JSON.parse(res);
        console.log(res);
        if(res.post_count>0){
          $('ul.collection').empty().html(res.html);
          $('input[name="search-page"]').val(2);
        } else {
          $('ul.collection').find('.empty').remove();
          $('ul.collection').empty().html('<li class="collection-item job slide-fade-up empty"><!-- INSERT NO RESULTS MESSAGE HERE --></li>');
        }
        $('.loading').addClass('hide');


      })
    });

    initializeInfiniteLoad();


    function initializeInfiniteLoad() {

      var button = jQuery('.loading');

      var page = 1;
      var loading = false;
      var scrollHandling = {
        allow: true,
        reallow: function () {
          scrollHandling.allow = true;
        },
        delay: 1 //(milliseconds) adjust to the highest acceptable value
      };

      $(window).scroll(function () {
        if ( !loading && scrollHandling.allow ) {
          scrollHandling.allow = false;
          setTimeout(scrollHandling.reallow, scrollHandling.delay);
          var bottom = $(document).height() - $(window).height();
          var scrolled = $(window).scrollTop();
          var near_bottom_of_page = (scrolled === bottom || scrolled > (bottom-10)) ? true : false;

          if ( near_bottom_of_page ) {
            $('.slide-fade-up').removeClass('slide-fade-up');

            loading = true;
            $('.loading').removeClass('hide');


            near_bottom_of_page = false;

            var job_types = ($('select[name="search-type"]').val()) ? $('select[name="search-type"]').val() : '';
            var search_category = ($('select[name="search-category"]').val()) ? $('select[name="search-category"]').val() : '';
            var search_location = ($('select[name="search-category"]').val()) ? $('select[name="search-category"]').val() : '';
            job_types = (job_types) ? job_types.join() : job_types;
            search_category = (search_category) ? search_category.join() : search_category;
            search_location = (search_location) ? search_location.join() : search_location;

            var page = $('input[name="search-page"]').val();

            var data = {
                        action: 'plugin2theme_job_search',
                        job_types: job_types,
                        search_category: search_category,
                        search_location: search_location,
                        page: parseInt(page)
                       };
            console.log('data sent: ',data);
            $.post(localized.ajax_url, data, function(res) {
              var res = JSON.parse(res);
              console.log(res);
              if(res.post_count>0){
                $('ul.collection').append(res.html);
                var items = $('li.slide-fade-up');
                TweenMax.staggerFrom(items, .25, {y: 100,opacity:.25}, 0.1)

                // UPDATE PAGE SINCE QUERY HASNT CHANGED
                var new_page_value = parseInt(page) + 1;
                loading = false;
                near_bottom_of_page = false;
                $('input[name="search-page"]').val(new_page_value);
              } else {
                loading = false;
                near_bottom_of_page = false;
                $('ul.collection').find('.empty').remove();
                $('ul.collection').append("<li class='collection-item job slide-fade-up empty'><!-- INSERT NO RESULTS MESSAGE HERE --></li>");
              }
              $('.loading').addClass('hide');

            });

          }
        }
      });

    }

  }); // end of document ready
})(jQuery); // end of jQuery name space


