<?php
/**
 * The header for our theme
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-124657442-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-124657442-1');
  </script>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-8374967389382615",
      enable_page_level_ads: true
    });
  </script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
  <!-- NAVIGATION -->
  <nav class="green" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="<?php echo site_url(); ?>" class="brand-logo center"><img width="180" src="<?= get_theme_file_uri('/images/white_logo_transparent_background.png') ?>"/></a>

      <!-- MENU CODE -->
      <!--		<ul class="right hide-on-med-and-down">-->
      <!--			--><?php //wp_nav_menu(array('theme_location'=>'main')); ?>
      <!--		</ul>-->
      <!--		<ul id="nav-mobile" class="sidenav">-->
      <!--			--><?php //wp_nav_menu(array('theme_location'=>'main')); ?>
      <!--		</ul>-->
      <!--		<a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>-->

    </div>
  </nav>

  <?php if(is_front_page()): ?>
  <!-- HERO -->
  <div class="container white">
    <div id="index-banner">
      <div class="section">
        <div class="container">
            <h1 class="header center green-text">
              Student or recent grad?<br />Get to Work.
            </h1>
            <div class="row center">
              <h5 class="header col s12 black-text">Your one stop destination for all engineering students and recent graduates to find internships, entry level jobs and career resources in different disciplines.</h5>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>