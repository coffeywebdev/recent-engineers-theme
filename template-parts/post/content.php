<?php
	/**
	 * Template part for displaying posts
	 *
	 * @link https://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package WordPress
	 * @subpackage Twenty_Seventeen
	 * @since 1.0
	 * @version 1.2
	 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container">

		<header class="entry-header">
			<?php
				if ( 'post' === get_post_type() ) {
					echo '<div class="entry-meta">';
					if ( is_single() ) {
						// posted on function
					} else {
						// // date / time
						// edit post link
					};
					echo '</div><!-- .entry-meta -->';
				};

				if ( is_single() ) {
					the_title( '<h1 class="header black-text">', '</h1>' );
				} elseif ( is_front_page() && is_home() ) {
					the_title( '<h3 class="header"><a class="black-text" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
				} else {
					the_title( '<h2 class="header"><a class="black-text" href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				}
			?>
		</header><!-- .entry-header -->

		<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
			<div class="post-thumbnail">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail(); ?>
				</a>
			</div><!-- .post-thumbnail -->
		<?php endif; ?>

		<div class="entry-content">
			<?php
				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
					get_the_title()
				) );
			?>
		</div><!-- .entry-content -->

		<?php
			if ( is_single() ) {
				 // single view footer
			}
		?>
	</div>

</article><!-- #post-## -->
