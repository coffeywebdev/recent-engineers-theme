<section class="no-results not-found">
	<header>
		<h1 class="header"><?php _e( 'Nothing Found', 'recent-engineers' ); ?></h1>
	</header>
	<div class="page-content">
		<p>
			<?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'recent-engineers' ); ?>
		</p>
		<?php get_search_form(); ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
