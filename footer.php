<footer class="page-footer green">
	<div class="container">
    <div class="row">
      <div class="left">
        <p class="center-align white-text">Copyright &copy; 2018 Recent Engineers. All Rights Reserved.</p>
      </div>
      <ul id="footer-links" class="footer-links right">
        <?php wp_nav_menu(array('theme_location'=>'footer')); ?>
      </ul>
    </div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
